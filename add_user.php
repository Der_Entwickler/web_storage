<?php

require_once 'login_credit.php';
require_once 'sanitize.php';
require_once 'header.php';

$connection = new mysqli($hn,$un,$pw,$db);
if($connection->connect_error) die('Fatal Error');

if(!empty($_POST['username'])) $username = sanitizeStringForSQL($connection,$_POST['username']);
if(!empty($_POST['password'])) $password = sanitizeStringForSQL($connection,$_POST['password']);
$password_hash = password_hash($password,PASSWORD_DEFAULT);

if($_POST['inv_code'] == 'q123'){

    $statement = $connection->prepare("INSERT INTO users VALUES(NULL,?,?)");

    $statement->bind_param('ss',$username,$password_hash);

    $result = $statement->execute();

    if(!$result){
        echo <<<_TEXT

        <div id='welcome_box_container'>

            <h1 id ='welcome_box_content'>

            Username already been registered or<br>
            something went wrong<br>

            <br><br>
            <a href = "register.php"> Okey </a>

            </h1>

        </div>

        _TEXT;
    }else{
        mkdir("./storage/".$username);
        echo <<<_TEXT

        <div id='full_container'>

            <h1 class='box'>

                You have either hacked this page<br>
                or got invited from the owner<br>
                Welcome, I guess, don't destroy this page though<br>
                <br>
                Press <a href='login.php'>here</a> to log in
                <br>

            </h1>

        <div>


        _TEXT;
    }
}else{

    echo <<<_TEXT

        <div id='full_container'>

            <h1 style='margin:auto' class='box'>

                Wrong Invitation Code
                <br>
                Press <a href='register.php'>here</a> to go back to registeration form.
                <br>

            </h1>

        <div>


        _TEXT;


}

?>