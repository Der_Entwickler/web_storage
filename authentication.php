<?php
session_start();

require_once 'login_credit.php';
require_once 'sanitize.php';
require_once 'header.php';

$connection = new mysqli($hn,$un,$pw,$db);

if($connection->connect_error) die('Fatal Error');

if(!empty($_POST['username']) && !empty($_POST['password'])){
    
    $username = sanitizeStringForSQL($connection,$_POST['username']);
    $password = sanitizeStringForSQL($connection,$_POST['password']);

    $statement = $connection->prepare('SELECT * FROM users WHERE Username = ?');

    $statement->bind_param('s',$username);

    $result = $statement->execute();

    if(!$result) die('Can not execute SQL statement');
    else{
        $data = $statement->get_result();
        $row = $data->fetch_assoc();

        if(!$row) echo "
        <script>
                    alert(\"USER incorect!\");
        </script>
        <div class='wrapper_center'>     
        <div id='welcome_box_container'>

            <h1 id='welcome_content'>Incorect Username<br></h1>
            Click <a href = 'login.php'>here</a> to continue

        </div>
        </div>
        </body>
        ";


        else{
            if(password_verify($password,$row['PasswordHash'])){

                echo <<<_TEXT
                <div class='wrapper_center'>
                    <div id='welcome_box_container'>
                        <h1 id='welcome_content'>Welcome<br>
                        $username<br>
                        <p><a href='storage_ui.php'>Click here to continue</a></p>
                        </h1>
                    </div>
                </div>

                _TEXT;

                $_SESSION['username'] = $username;
                $_SESSION['check'] = hash('sha3-256', $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT']);

            }else{
                echo "<script>
                        alert(\"Password incorect!\");
                    </script>
                    <div class='wrapper_center'>
                    <div id='welcome_box_container'>

                        <h1 id='welcome_content'>Wrong password<br>
                        
                        Click <a href='login.php'>here</a> to continue

                        </h1>
                    </div>
                </div>

                </body>

                ";
            }
        }
    }
}else{
    echo "<script>
                    alert(\"Fill both your username and password\");
        </script>
        <div class='wrapper_center'>
        <div id='welcome_box_container'>

            <h1 id='welcome_content'>Invaild Input<br>
                        
                Click <a href='login.php'>here</a> to continue

            </h1>
        </div>
        </div>
        
        ";
}



?>