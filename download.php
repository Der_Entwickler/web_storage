<?php

session_start();

require_once 'zip.php';

if(isset($_GET['filename']) && isset($_SESSION['username'])){

    $path_parts = pathinfo($_GET['filename']);
    $file_name = $path_parts['basename'];
    $file_path  = './storage/'. $_SESSION['username'] . '/' . $file_name;

    if(file_exists($file_path)){
        
        if(filetype($file_path) != 'dir'){

            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary"); 
            header("Content-disposition: attachment; filename=\"$file_name\"");
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_path));

            readfile($file_path); 

        }
        else{
            new GoodZipArchive($file_path,'./storage/'. $_SESSION['username'] . "/" . $file_name. ".zip");

            $file_path = './storage/'. $_SESSION['username'] . "/" . $file_name. ".zip";
            $file_name = $file_name. ".zip";

            header('Content-Type: application/octet-stream');
            header("Content-Transfer-Encoding: Binary"); 
            header("Content-disposition: attachment; filename=\"$file_name\"");
            header('Connection: Keep-Alive');
            header('Expires: 0');
            header('Pragma: public');
            header('Content-Length: ' . filesize($file_path));
            
            readfile($file_path);

            if(file_exists($file_path) || connection_aborted()) unlink($file_path);
        }
    }
}
?>