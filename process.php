<?php
session_start();

if (!empty($_SESSION['username'])){
    $fp = fopen('loginfo.txt','a+');

    $user = $_SESSION['username']; 

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_FILES['file0'])) {
            $errors = [];
            $path = "./storage/$user/";
            
            $i = 0;

            while ($_FILES["file".$i]["tmp_name"]) {

                $file_tmp = $_FILES["file".$i]["tmp_name"];
                $file_size = $_FILES["file".$i]['size'];

                $folderpath = $path.dirname($_POST["filepath".$i]);
                $file = $path.dirname($_POST["filepath".$i]).'/'.basename($_FILES['file'.$i]['name']);

                $create = mkdir($folderpath,0644,true);
                fwrite($fp,"\ncreate: ".$create);
                $move = move_uploaded_file($file_tmp,$file);
                fwrite($fp,"\nmove: ".$move);
                $i++;

            }
        fclose($fp);
    if ($errors) print_r($errors);
        }
    }
}

header("Location: storage_ui.php");