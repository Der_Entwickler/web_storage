<?php

session_start();

if(isset($_GET['filename']) && isset($_SESSION['username'])){

    $path_parts = pathinfo($_GET['filename']);
    $file_name = $path_parts['basename'];
    $file_path  = './storage/'. $_SESSION['username'] . "/$file_name";

    if(is_dir($file_path)) delete_files($file_path);
    else unlink($file_path);

}

header("Location: storage_ui.php"); 
exit();

function delete_files($target) {
    if(is_dir($target)){
        $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned

        foreach( $files as $file ){
            delete_files( $file );      
        }

        rmdir( $target );
    } elseif(is_file($target)) {
        unlink( $target );  
    }
}    

?>