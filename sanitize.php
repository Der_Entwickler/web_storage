<?php

function sanitizeString($var){
    
    $var = stripslashes($var);
    $var = strip_tags($var);
    $var = htmlentities($var);

    return $var;

}

function sanitizeStringForSQL($connection,$var){
    return mysqli_real_escape_string($connection, sanitizeString($var));
}
?>