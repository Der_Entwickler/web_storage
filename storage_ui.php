<?php
session_start();

require_once "header.php";

if (!empty(($_SESSION['username'])))
{

    $user = $_SESSION['username'];

    if ($_SESSION['check'] != hash('sha3-256', $_SERVER['REMOTE_ADDR'] . $_SERVER['HTTP_USER_AGENT'])) different_user();

    else if ( !isset($_SESSION['init']) )
    {
        session_regenerate_id();
        $_SESSION['init'] = 1;

    }

    $myDirectory=opendir("./storage/".$_SESSION['username']);
    while($entryName=readdir($myDirectory)) {
        if ($entryName != '.' && $entryName != '..') $dirArray[]=$entryName;
    }
    closedir($myDirectory);

    if($dirArray != NULL) $indexCount = count($dirArray);
    else $indexCount = 0;


    echo <<<_TEXT
            <div class='wrapper_center'>
                <table id="upload_table" cellpadding='15px'>
                    <caption>
                        $user's storage
                    </caption>
                    <thead>
                        <tr>
                            <th style="width:25%">File</th>
                            <th style="width:25%">Size</th>
                            <th colspan="2">Action</th>
                        </tr>
                    </thread>
                    <tbody class = 'table_body'>
    _TEXT;

    for ($i = 0; $i < $indexCount; $i++)
    {
        $filename = $dirArray[$i];
        $size = number_format(filesize("./storage/$user/$filename"));
        
        echo <<<_TEXT
          <tr>
            <td><a href="./storage/$user/$filename">$filename</a></td>
            <td>$size bytes</td>
            <td><a href="download.php?filename=$filename">Download</a></td>
            <td><a href="remove.php?filename=$filename">Remove</a></td>
          </tr>
        _TEXT;    
    }

    echo <<<_TEXT
    </tbody>
    <tfoot>
        <tr>
            <td style='text-align: center' colspan='4'>
                <form id='upload_form' enctype="multipart/form-data">
                    <input type="hidden" name="MAX_FILE_SIZE" value="800000000" />
                    Select
                    <input class='for_upload' name='file_upload' type="file" id='file_input' style="display:none;" multiple>
                    <input type="button" value="File(s)" onclick="document.getElementById('file_input').click();" >
                    Or
                    <input class='for_upload' name='folder_upload' type="file" id='folder_input' style="display:none;" webkitdirectory mozdirectory directory multiple>
                    <input type="button" value="Folder(s)"  onclick="document.getElementById('folder_input').click();" >
                    To
                    <input type="submit" value="Upload" name="submit">
                    <progress id="upload_progress" value="0" max="100"></progress>
                    <script src='upload.js'></script>
                </form>
            </td>
        </tr>
    </tfoot>
    </div>
    </table>
    </body>

    _TEXT;

}

function different_user(){

    echo <<<_TEXT

    <div class="wrapper_center">

        <h1> Session changed, please log in again 
        <a href = "login.php" > here </a>
        </h1>

    </div>

    _TEXT;

    exit(0);
}

?>