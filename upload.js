upload_form = document.getElementById('upload_form');

upload_form.addEventListener('submit', e => {
    e.preventDefault();

    const formData = new FormData();

    var all_files = [];

    const normal_files = document.querySelector("[name='file_upload']").files;
    
    for (let i = 0; i < normal_files.length; i++) {
        all_files.push(normal_files[i]);
    }

    const folder_files = document.querySelector("[name='folder_upload']").files;

    for (let j = 0; j < folder_files.length; j++) {
        all_files.push(folder_files[j]);
    }

    console.log(all_files.length);
    for (let k = 0; k < all_files.length; k++) {   
        let file = all_files[k];
        let fileParamName = `file${k}`;
        let filePathParamName = `filepath${k}`;
        console.log(filePathParamName);
        formData.append(fileParamName, file);
        formData.append(filePathParamName,file.webkitRelativePath);
    }

    $.ajax({
        url: 'process.php',
        data: formData,
        processData: false,
        contentType: false,
        type: 'POST',
        // this part is for progress bar
        xhr: function () {
            var xhr = new window.XMLHttpRequest();
            xhr.upload.addEventListener("progress", function (evt) {
                if (evt.lengthComputable) {
                    var percentComplete = evt.loaded / evt.total;
                    percentComplete = parseInt(percentComplete * 100);
                    $('#upload_progress').val(percentComplete);
                    $('#upload_progress').css('display', 'inline');
                }
            }, false);
            return xhr;
        },
        success: function (data) {
            $('#upload_progress').css('display', 'none');
            location.reload(true);
        }
    });
},true);